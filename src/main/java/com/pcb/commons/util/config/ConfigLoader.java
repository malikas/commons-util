package com.pcb.commons.util.config;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ConfigurationConverter;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.Objects;
import java.util.Properties;

public enum ConfigLoader {
    INSTANCE;

    private static final Logger logger = LoggerFactory.getLogger(ConfigLoader.class);

    private final String configPath;

    ConfigLoader() {
        this.configPath = System.getProperty("configPath") != null ? System.getProperty("configPath") : "./";
    }

    /**
     * Used to build org.apache.commons.configuration2.Configuration from the file.
     *
     * @param fileName The file name used to build Configuration
     * @return org.apache.commons.configuration2.Configuration
     * @throws RuntimeException If an exception occurred while trying to build Configuration
     */
    public static Configuration getConfigsFromFile(@Nonnull String fileName) throws RuntimeException {
        Objects.requireNonNull(fileName);

        logger.debug("Creating configs from a file: {}", fileName);
        Configurations configurations = new Configurations();
        Configuration configs;
        try {
            configs = configurations.properties(new File(INSTANCE.configPath + fileName));
        } catch (ConfigurationException e) {
            logger.error("Encountered an error while loading configs from a file: {}", fileName, e);
            throw new RuntimeException(e);
        }

        return configs;
    }

    /**
     * Used to build java.util.Properties from the file.
     *
     * @param fileName The file name used to build Properties
     * @return java.util.Properties
     * @throws RuntimeException If an exception occurred while trying to build Properties
     */
    public static Properties getPropertiesFromFile(@Nonnull String fileName) throws RuntimeException {
        Objects.requireNonNull(fileName);

        Configuration configs = getConfigsFromFile(fileName);
        if (configs == null) {
            return new Properties();
        }

        return ConfigurationConverter.getProperties(configs);
    }
}
