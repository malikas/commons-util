package com.pcb.commons.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fasterxml.jackson.datatype.jsonorg.JsonOrgModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.util.Random;
import java.util.TimeZone;

/**
 * JacksonMappingFactory is a factory helper that provides the means of requesting a thread-safe
 * instance of Jackson's ObjectMapper for all JSON (de)serialization needs.
 */
public class JacksonMappingFactory {
    private static final int POOL_SIZE = 5;

    private static ObjectMapper[] mappers;
    private static Random randomGenerator;

    private JacksonMappingFactory() {
    }

    static {
        mappers = new ObjectMapper[POOL_SIZE];
        for (int i = 0; i < POOL_SIZE; i++) {
            mappers[i] = new ObjectMapper()
                    .setTimeZone(TimeZone.getDefault())
                    .registerModule(new JsonOrgModule())
                    .registerModule(new JodaModule())
                    .registerModule(new JavaTimeModule())
                    .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                    .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                    .enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
                    .configure(JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS, true);
        }
        randomGenerator = new Random();
    }

    public static ObjectMapper getMapper() {
        return mappers[randomGenerator.nextInt(POOL_SIZE)];
    }
}
